/**
 * Copyright (c) 2013 Kinvey Inc. Licensed under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * 
 */
package com.example.testdrive.android;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.testdrive.android.model.Entity;
import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.android.callback.KinveyUserCallback;
import com.kinvey.java.Query;
import com.kinvey.java.User;
import com.kinvey.java.cache.CachePolicy;
import com.kinvey.java.cache.InMemoryLRUCache;
import com.kinvey.java.core.KinveyClientCallback;

public class TestDrive extends Activity {

    public static final String TAG = "TestDrive";

    private ProgressBar bar;

    private String appKey = "kid_VezCbCCcTq";
    private String appSecret = "5c605948d9f04bde8eaf6864146fe078";

    private Client kinveyClient;
    AsyncAppData<Entity> appData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_drive);
        bar = (ProgressBar) findViewById(R.id.refresh_progress);
        bar.setIndeterminate(true);

        kinveyClient = new Client.Builder(appKey, appSecret, this).build();
        appData = kinveyClient.appData("entityCollection", Entity.class);
//        appData.setOffline(OfflinePolicy.ONLINE_FIRST,
//                new SqlLiteOfflineStore<Entity>(this.getApplicationContext()));
        appData.setCache(new InMemoryLRUCache<String, String>(), CachePolicy.CACHEFIRST);
        bar.setVisibility(View.VISIBLE);
        kinveyClient.user().login("testuser", "123456",
                new KinveyUserCallback() {
                    @Override
                    public void onSuccess(User result) {
                        bar.setVisibility(View.GONE);
                        Log.i(TAG,
                                "Logged in successfully as "
                                        + result.getUsername());
                        Toast.makeText(
                                TestDrive.this,
                                "New user logged in successfully as "
                                        + result.getUsername(),
                                Toast.LENGTH_LONG).show();
                        Log.v(TAG, result.toString());
                    }

                    @Override
                    public void onFailure(Throwable error) {
                        bar.setVisibility(View.GONE);
                        Log.e(TAG, "Login Failure", error);
                        Toast.makeText(TestDrive.this,
                                "Login error: " + error.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void onLoadAllClick(View view) {
        bar.setVisibility(View.VISIBLE);
        appData.get(new Query(), new KinveyListCallback<Entity>() {
            @Override
            public void onSuccess(Entity[] result) {
                bar.setVisibility(View.GONE);
                String msg = null;
                if(result == null)
                    msg = "No data retrieved.";
                else
                    msg = "Retrieved total :" + result.length;
                Toast.makeText(TestDrive.this, msg, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Throwable error) {
                bar.setVisibility(View.GONE);
                Log.e(TAG, "AppData.get all Failure", error);
                Toast.makeText(TestDrive.this,
                        "Get All error: " + error.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onSaveClick(View view) {
        bar.setVisibility(View.VISIBLE);
        long now = System.currentTimeMillis();
        Entity entity = new Entity("myEntity-" + now, "Descrption-"+now);
        appData.save(entity, new KinveyClientCallback<Entity>() {
            @Override
            public void onSuccess(Entity result) {
                bar.setVisibility(View.GONE);
                Log.v(TAG, "saved");
                Toast.makeText(TestDrive.this,
                        "Entity Saved\nTitle: " + result.getTitle(),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Throwable error) {
                bar.setVisibility(View.GONE);
                Log.e(TAG, "AppData.save Failure", error);
                Toast.makeText(TestDrive.this,
                        "Save All error: " + error.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

}